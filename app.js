var app = angular.module('myApp', ['ngRoute']);

app.config(function($routeProvider) {
  $routeProvider

  .when('/', {
    templateUrl : 'pages/home.html',
    // controller  : 'HomeController'
  })

  .when('/total', {
    templateUrl : 'pages/total.html',
    controller  : 'TotalController'
  })

  .otherwise({redirectTo: '/'});
});

var res=0;
app.controller('items', ['$scope', '$location',  function($scope, $location) {

    $scope.go = function ( path ) {
        res = (($scope.price1 * $scope.qty1)+($scope.price2 * $scope.qty2)+($scope.price3 * $scope.qty3)+($scope.price4 * $scope.qty4)) ;
        //console.log(res)
        $location.path( path );
      };
}]);

app.controller('TotalController', function($scope) {
  
  $scope.message =  res;
});


